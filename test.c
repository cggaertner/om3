#undef NDEBUG
#include "om3lib.h"

#include <math.h>
#include <string.h>

static om3var Point_init(size_t count, om3var *args)
{
    assert(count == 3);

    om3obj *self = om3_unbox_obj(*args);
    double *x = om3_unref_float(om3_seek(self, "x"));
    double *y = om3_unref_float(om3_seek(self, "y"));

    *x = om3_unbox_float(args[1]);
    *y = om3_unbox_float(args[2]);

    return *args;
}

static om3var Point_len(size_t count, om3var *args)
{
    assert(count == 1);

    om3obj *self = om3_unbox_obj(*args);
    double *x = om3_unref_float(om3_seek(self, "x"));
    double *y = om3_unref_float(om3_seek(self, "y"));

    return om3_box_float(sqrt(*x * *x + *y * *y));
}

static om3var Point_scale(size_t count, om3var *args)
{
    assert(count == 2);

    om3obj *self = om3_unbox_obj(args[0]);
    double factor = om3_unbox_float(args[1]);

    double *x = om3_unref_float(om3_seek(self, "x"));
    double *y = om3_unref_float(om3_seek(self, "y"));

    *x *= factor;
    *y *= factor;

    return *args;
}

int main(void)
{
    om3_lib_init(NULL);

    om3obj *Point = om3_unbox_obj(om3_invoke(om3.Class, 6, (om3var []){
        om3_invoke(om3.Method, 1, (om3var []){ om3_box_fptr(Point_init) }),
        om3_invoke(om3.Tuple, 3, (om3var []){
            om3_box_string("classname"),
            om3_box_string("Point"),
            om3_box_int(OM3_STATIC),
        }),
        om3_invoke(om3.Tuple, 3, (om3var []){
            om3_box_string("x"),
            om3_box_float(0),
            om3_box_int(OM3_DYNAMIC),
        }),
        om3_invoke(om3.Tuple, 3, (om3var []){
            om3_box_string("y"),
            om3_box_float(0),
            om3_box_int(OM3_DYNAMIC),
        }),
        om3_invoke(om3.Tuple, 3, (om3var []){
            om3_box_string("len"),
            om3_invoke(om3.Method, 1, (om3var []){ om3_box_fptr(Point_len) }),
            om3_box_int(OM3_VIRTUAL),
        }),
        om3_invoke(om3.Tuple, 3, (om3var []){
            om3_box_string("scale"),
            om3_invoke(om3.Method, 1, (om3var []){ om3_box_fptr(Point_scale) }),
            om3_box_int(OM3_METHOD),
        }),
    }));

    {
        const char *name =
            om3_unbox_string(om3_eval(om3_seek(Point, "classname")));
        assert(strcmp(name, "Point") == 0);
    }

    {
        om3obj *point = om3_unbox_obj(om3_invoke(Point, 3, (om3var []){
            om3_nil, om3_box_float(2), om3_box_float(1.5)
        }));

        double *x = om3_unref_float(om3_seek(point, "x"));
        double *y = om3_unref_float(om3_seek(point, "y"));
        assert(*x == 2);
        assert(*y == 1.5);

        double len = om3_unbox_float(om3_eval(om3_seek(point, "len")));
        assert(len == 2.5);

        om3_call(om3_seek(point, "scale"), 2, (om3var []){
            om3_nil, om3_box_float(2)
        });
        assert(*x == 4);
        assert(*y == 3);

        om3obj *method = om3_unbox_obj(om3_eval(om3_seek(point, "scale")));
        om3_invoke(method, 2, (om3var []){
            om3_box_obj(point), om3_box_float(0.5)
        });
        assert(*x == 2);
        assert(*y == 1.5);
    }

    return 0;
}
