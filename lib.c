#include "om3lib.h"

#include <stdlib.h>

static const om3ref nullref;
om3lib om3;

void om3_lib_init(om3lib *lib)
{
    if(!lib) lib = &om3;
    om3_lib_init_tuple(&lib->Tuple);
    om3_lib_init_class(&lib->Class);
    om3_lib_init_method(&lib->Method);
}

om3ref om3_lib_fail_seek(om3obj *obj, const char *name)
{
    (void)obj, (void)name;
    assert(!"not supported");
    return nullref;
}

om3var om3_lib_fail_load(om3obj *obj, om3var key)
{
    (void)obj, (void)key;
    assert(!"not supported");
    return om3_nil;
}

void om3_lib_fail_store(om3obj *obj, om3var key, om3var value)
{
    (void)obj, (void)key, (void)value;
    assert(!"not supported");
}

om3var om3_lib_fail_invoke(om3obj *obj, size_t count, om3var *args)
{
    (void)obj, (void)count, (void)args;
    assert(!"not supported");
    return om3_nil;
}

void om3_lib_fail_destroy(om3obj *obj)
{
    (void)obj;
    assert(!"not supported");
}

void om3_lib_destroy_nothing(om3obj *obj)
{
    (void)obj;
}

void om3_lib_destroy_storage(om3obj *obj)
{
    free(obj);
}
