#include "om3lib.h"

#include <stdlib.h>
#include <string.h>

struct instance
{
    om3obj header;
    struct meta *meta;
    om3val slots[];
};

struct info
{
    const char *name;
    size_t index;
    const om3ref_vt *vt;
    _Bool dynamic;
};

struct meta
{
    struct instance *class;
    struct instance *proto;
    om3obj *init;
    size_t size;
    size_t count;
    struct info map[];
};

static const om3ref_vt *get_value_ref_vt(om3type type)
{
    switch(type)
    {
        case OM3_BOOL: return &OM3_BOOL_REF;
        case OM3_INT: return &OM3_INT_REF;
        case OM3_UINT: return &OM3_UINT_REF;
        case OM3_FLOAT: return &OM3_FLOAT_REF;
        case OM3_STRING: return &OM3_STRING_REF;
        case OM3_PTR: return &OM3_PTR_REF;
        case OM3_FPTR: return &OM3_FPTR_REF;
        case OM3_OBJ: return &OM3_OBJ_REF;

        case OM3_NIL:
        default:
            assert(!"illegal type");
            return NULL;
    }
}

static om3var eval_virtual(om3obj *self, void *ptr)
{
    om3obj *proxy = *(om3obj **)ptr;
    om3var args[] = { om3_box_obj(self) };
    return om3_invoke(proxy, 1, args);
}

static void assign_virtual(om3obj *self, void *ptr, om3var var)
{
    om3obj *proxy = *(om3obj **)ptr;
    om3var args[] = { om3_box_obj(self), var };
    om3_invoke(proxy, 2, args);
}

static om3var call_virtual(om3obj *obj, void *ptr, size_t count, om3var *args)
{
    (void)obj, (void)ptr, (void)count, (void)args;
    assert(!"not supported");
    return om3_nil;
}

static om3var eval_method(om3obj *self, void *ptr)
{
    (void)self;
    return om3_box_obj(*(om3obj **)ptr);
}

static void assign_method(om3obj *self, void *ptr, om3var var)
{
    (void)self;
    *(om3obj **)ptr = om3_unbox_obj(var);
}

static om3var call_method(om3obj *self, void *ptr, size_t count, om3var *args)
{
    assert(count > 0);
    assert(args->type == OM3_NIL);
    *args = om3_box_obj(self);
    return om3_invoke(*(om3obj **)ptr, count, args);
}

static int cmp(const void *key, const void *value)
{
    return strcmp((const char *)key, *(const char **)value);
}

static struct info *get_info(struct meta *meta, const char *name)
{
    return bsearch(name, meta->map, meta->count, sizeof *meta->map, cmp);
}

static om3ref seek_instance_slot(om3obj *obj, const char *name)
{
    struct instance *self = (struct instance *)obj;
    struct meta *meta = self->meta;
    struct info *info = get_info(meta, name);

    assert(info);

    om3val *slots = info->dynamic ? self->slots : meta->class->slots;

    return (om3ref){
        .vt = info->vt,
        .self = obj,
        .ptr = slots + info->index,
    };
}

static om3ref seek_class_slot(om3obj *obj, const char *name)
{
    struct instance *self = (struct instance *)obj;
    struct meta *meta = self->meta;
    struct info *info = get_info(meta, name);

    assert(info);

    om3val *slots = info->dynamic ? meta->proto->slots : self->slots;

    return (om3ref){
        .vt = info->vt,
        .self = obj,
        .ptr = slots + info->index,
    };
}

static om3var invoke_class(om3obj *obj, size_t count, om3var *args)
{
    struct instance *class = (struct instance *)obj;
    struct meta *meta = class->meta;
    struct instance *instance = malloc(meta->size);
    memcpy(instance, meta->proto, meta->size);

    om3var var = om3_box_obj(&instance->header);
    if(!count) return var;

    assert(args->type == OM3_NIL);
    *args = var;
    return om3_invoke(meta->init, count, args);
}

static void destroy_class(om3obj *obj)
{
    struct instance *class = (struct instance *)obj;
    free(class->meta);
    free(class);
}

static om3var invoke_type(om3obj *obj, size_t count, om3var *args)
{
    static const om3obj_vt CLASS_VT = {
        seek_class_slot,
        om3_lib_fail_load,
        om3_lib_fail_store,
        invoke_class,
        destroy_class,
    };

    static const om3obj_vt INSTANCE_VT = {
        seek_instance_slot,
        om3_lib_fail_load,
        om3_lib_fail_store,
        om3_lib_fail_invoke,
        om3_lib_destroy_storage,
    };

    static const om3ref_vt VIRTUAL_REF_VT = {
        eval_virtual,
        assign_virtual,
        call_virtual,
    };

    static const om3ref_vt METHOD_REF_VT = {
        eval_method,
        assign_method,
        call_method,
    };

    (void)obj;
    assert(count > 1);

    om3obj *init = om3_unbox_obj(*args);
    ++args, --count;

    struct meta *meta = malloc(sizeof *meta + count * sizeof *meta->map);

    size_t dynamic_count = 0;
    size_t static_count = 0;

    for(size_t i = 0; i < count; ++i)
    {
        om3obj *slot = om3_unbox_obj(args[i]);

        const char *name = om3_unbox_string(om3_load(slot, om3_box_uint(0)));
        om3type type = om3_load(slot, om3_box_uint(1)).type;
        int64_t mode = om3_unbox_int(om3_load(slot, om3_box_uint(2)));

        size_t pos = i;

        // insertion sort
        while(pos > 0 && strcmp(name, meta->map[pos - 1].name) < 0) {
            meta->map[pos] = meta->map[pos - 1];
            --pos;
        }

        meta->map[pos].name = name;

        switch(mode)
        {
            case OM3_DYNAMIC:
                meta->map[pos].vt = get_value_ref_vt(type);
                meta->map[pos].index = dynamic_count++;
                meta->map[pos].dynamic = 1;
                break;
            case OM3_STATIC:
                meta->map[pos].vt = get_value_ref_vt(type);
                meta->map[pos].index = static_count++;
                meta->map[pos].dynamic = 0;
                break;
            case OM3_VIRTUAL:
                assert(type == OM3_OBJ);
                meta->map[pos].vt = &VIRTUAL_REF_VT;
                meta->map[pos].index = static_count++;
                meta->map[pos].dynamic = 0;
                break;
            case OM3_METHOD:
                assert(type == OM3_OBJ);
                meta->map[pos].vt = &METHOD_REF_VT;
                meta->map[pos].index = static_count++;
                meta->map[pos].dynamic = 0;
                break;
            default:
                assert(!"illegal slot access mode");
                return om3_nil;
        }
    }

    size_t class_size = sizeof (struct instance) +
        static_count * sizeof (om3val);
    size_t proto_size = sizeof (struct instance) +
        dynamic_count * sizeof (om3val);

    struct instance *class = malloc(class_size);
    struct instance *proto = malloc(proto_size);

    meta->class = class;
    meta->proto = proto;
    meta->init = init;
    meta->size = proto_size;
    meta->count = count;

    class->header.vt = &CLASS_VT;
    class->meta = meta;

    proto->header.vt = &INSTANCE_VT;
    proto->meta = meta;

    for(size_t i = 0, class_i = 0, proto_i = 0; i < count; ++i)
    {
        om3obj *slot = om3_unbox_obj(args[i]);
        int64_t mode = om3_unbox_int(om3_load(slot, om3_box_uint(2)));
        om3val value = om3_load(slot, om3_box_uint(1)).body;

        if(mode == OM3_DYNAMIC)
            proto->slots[proto_i++] = value;
        else class->slots[class_i++] = value;
    }

    return om3_box_obj(&meta->class->header);
}

void om3_lib_init_class(om3obj **slot)
{
    static const om3obj_vt VT = {
        om3_lib_fail_seek,
        om3_lib_fail_load,
        om3_lib_fail_store,
        invoke_type,
        om3_lib_destroy_nothing,
    };

    static om3obj OBJ = { &VT };

    *slot = &OBJ;
}
