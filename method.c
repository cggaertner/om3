#include "om3lib.h"

#include <stdlib.h>

struct method
{
    om3obj header;
    om3func *fp;
};

static om3var invoke_method(om3obj *obj, size_t count, om3var *args)
{
    struct method *method = (struct method *)obj;
    return method->fp(count, args);
}

static om3var invoke_type(om3obj *obj, size_t count, om3var *args)
{
    static const om3obj_vt VT = {
        om3_lib_fail_seek,
        om3_lib_fail_load,
        om3_lib_fail_store,
        invoke_method,
        om3_lib_destroy_storage,
    };

    (void)obj;
    assert(count == 1);

    struct method *method = malloc(sizeof *method);
    method->header.vt = &VT;
    method->fp = om3_unbox_fptr(*args);

    return om3_box_obj(&method->header);
}

void om3_lib_init_method(om3obj **slot)
{
    static const om3obj_vt VT = {
        om3_lib_fail_seek,
        om3_lib_fail_load,
        om3_lib_fail_store,
        invoke_type,
        om3_lib_destroy_nothing,
    };

    static om3obj OBJ = { &VT };

    *slot = &OBJ;
}
