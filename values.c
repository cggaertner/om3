#include "om3.h"

#define def(VT, CT, BT) \
static om3var eval_ ## VT(om3obj *self, void *ptr) \
{ \
    (void)self; \
    return om3_box_ ## VT(*(CT *)ptr); \
} \
\
static void assign_ ## VT(om3obj *self, void *ptr, om3var var) \
{ \
    (void)self; \
    *(CT *)ptr = om3_unbox_ ## VT(var); \
} \
\
static om3var call_ ## VT(om3obj *obj, void *ptr, size_t count, om3var *args) \
{ \
    (void)obj, (void)ptr, (void)count, (void)args; \
    assert(!"cannot call value"); \
    return om3_nil; \
} \
\
const om3ref_vt OM3_ ## BT ## _REF = { \
    eval_ ## VT, \
    assign_ ## VT, \
    call_ ## VT, \
};

def(bool, _Bool, BOOL)
def(int, int64_t, INT)
def(uint, uint64_t, UINT)
def(float, double, FLOAT)
def(string, const char *, STRING)
def(ptr, void *, PTR)
def(fptr, om3func *, FPTR)
def(obj, om3obj *, OBJ)
