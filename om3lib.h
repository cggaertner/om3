#ifndef OM3LIB_H_
#define OM3LIB_H_

#include "om3.h"

enum
{
    OM3_DYNAMIC,
    OM3_STATIC,
    OM3_VIRTUAL,
    OM3_METHOD,
};

typedef struct om3lib om3lib;

struct om3lib
{
    om3obj *Tuple;
    om3obj *Class;
    om3obj *Method;
};

extern om3lib om3;
extern void om3_lib_init(om3lib *lib);

extern void om3_lib_init_tuple(om3obj **slot);
extern void om3_lib_init_class(om3obj **slot);
extern void om3_lib_init_method(om3obj **slot);

extern om3ref om3_lib_fail_seek(om3obj *obj, const char *name);
extern om3var om3_lib_fail_load(om3obj *obj, om3var key);
extern void om3_lib_fail_store(om3obj *obj, om3var key, om3var value);
extern om3var om3_lib_fail_invoke(om3obj *obj, size_t count, om3var *args);
extern void om3_lib_fail_destroy(om3obj *obj);
extern void om3_lib_destroy_nothing(om3obj *obj);
extern void om3_lib_destroy_storage(om3obj *obj);

#endif
