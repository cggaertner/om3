#ifndef OM3_H_
#define OM3_H_

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#define OM3_BOX_(T) return (om3var){ OM3_ ## T, { .T = val } };
#define OM3_UNBOX_(T) assert(var.type == OM3_ ## T); return var.body.T;
#define OM3_UNREF_(T) assert(ref.vt == &OM3_ ## T ## _REF); return ref.ptr;
#define OM3_CPTR_ OM3_PTR

enum om3type
{
    OM3_NIL,
    OM3_BOOL,
    OM3_INT,
    OM3_UINT,
    OM3_FLOAT,
    OM3_STRING,
    OM3_PTR,
    OM3_FPTR,
    OM3_OBJ,
};

typedef enum om3type om3type;
typedef struct om3var om3var;
typedef struct om3obj om3obj;
typedef union om3val om3val;
typedef struct om3ref om3ref;
typedef struct om3obj_vt om3obj_vt;
typedef struct om3ref_vt om3ref_vt;
typedef om3var om3func(size_t, om3var *);

union om3val
{
    _Bool BOOL;
    int64_t INT;
    uint64_t UINT;
    double FLOAT;
    const char *STRING;
    void *PTR;
    om3func *FPTR;
    om3obj *OBJ;
    const void *CPTR_;
};

struct om3var
{
    om3type type;
    om3val body;
};

struct om3obj
{
    const om3obj_vt *vt;
};

struct om3ref
{
    const om3ref_vt *vt;
    om3obj *self;
    void *ptr;
};

struct om3obj_vt
{
    om3ref (*seek)(om3obj *, const char *);
    om3var (*load)(om3obj *, om3var);
    void (*store)(om3obj *, om3var, om3var);
    om3var (*invoke)(om3obj *, size_t, om3var *);
    void (*destroy)(om3obj *);
};

struct om3ref_vt
{
    om3var (*eval)(om3obj *, void *);
    void (*assign)(om3obj *, void *, om3var);
    om3var (*call)(om3obj *, void *, size_t, om3var *);
};

static const om3var om3_nil;

extern const om3ref_vt OM3_BOOL_REF;
extern const om3ref_vt OM3_INT_REF;
extern const om3ref_vt OM3_UINT_REF;
extern const om3ref_vt OM3_FLOAT_REF;
extern const om3ref_vt OM3_STRING_REF;
extern const om3ref_vt OM3_PTR_REF;
extern const om3ref_vt OM3_FPTR_REF;
extern const om3ref_vt OM3_OBJ_REF;

static inline om3var om3_box_bool(_Bool val) { OM3_BOX_(BOOL) }
static inline om3var om3_box_int(int64_t val) { OM3_BOX_(INT) }
static inline om3var om3_box_uint(uint64_t val) { OM3_BOX_(UINT) }
static inline om3var om3_box_float(double val) { OM3_BOX_(FLOAT) }
static inline om3var om3_box_string(const char *val) { OM3_BOX_(STRING) }
static inline om3var om3_box_ptr(const void *val) { OM3_BOX_(CPTR_) }
static inline om3var om3_box_fptr(om3func *val) { OM3_BOX_(FPTR) }
static inline om3var om3_box_obj(om3obj *val) { OM3_BOX_(OBJ) }

static inline _Bool om3_unbox_bool(om3var var) { OM3_UNBOX_(BOOL) }
static inline int64_t om3_unbox_int(om3var var) { OM3_UNBOX_(INT) }
static inline uint64_t om3_unbox_uint(om3var var) { OM3_UNBOX_(UINT) }
static inline double om3_unbox_float(om3var var) { OM3_UNBOX_(FLOAT) }
static inline const char *om3_unbox_string(om3var var) { OM3_UNBOX_(STRING) }
static inline void *om3_unbox_ptr(om3var var) { OM3_UNBOX_(PTR) }
static inline om3func *om3_unbox_fptr(om3var var) { OM3_UNBOX_(FPTR) }
static inline om3obj *om3_unbox_obj(om3var var) { OM3_UNBOX_(OBJ) }

static inline _Bool *om3_unref_bool(om3ref ref) { OM3_UNREF_(BOOL) }
static inline int64_t *om3_unref_int(om3ref ref) { OM3_UNREF_(INT) }
static inline uint64_t *om3_unref_uint(om3ref ref) { OM3_UNREF_(UINT) }
static inline double *om3_unref_float(om3ref ref) { OM3_UNREF_(FLOAT) }
static inline const char **om3_unref_string(om3ref ref) { OM3_UNREF_(STRING) }
static inline void **om3_unref_ptr(om3ref ref) { OM3_UNREF_(PTR) }
static inline om3func **om3_unref_fptr(om3ref ref) { OM3_UNREF_(FPTR) }
static inline om3obj **om3_unref_obj(om3ref ref) { OM3_UNREF_(OBJ) }

static inline om3ref om3_seek(om3obj *self, const char *name)
{
    return self->vt->seek(self, name);
}

static inline om3var om3_load(om3obj *obj, om3var var)
{
    return obj->vt->load(obj, var);
}

static inline void om3_store(om3obj *obj, om3var key, om3var value)
{
    obj->vt->store(obj, key, value);
}

static inline om3var om3_invoke(om3obj *obj, size_t count, om3var *args)
{
    return obj->vt->invoke(obj, count, args);
}

static inline void om3_destroy(om3obj *obj)
{
    obj->vt->destroy(obj);
}

static inline om3var om3_eval(om3ref ref)
{
    return ref.vt->eval(ref.self, ref.ptr);
}

static inline void om3_assign(om3ref ref, om3var var)
{
    ref.vt->assign(ref.self, ref.ptr, var);
}

static inline om3var om3_call(om3ref ref, size_t count, om3var *args)
{
    return ref.vt->call(ref.self, ref.ptr, count, args);
}

#endif
