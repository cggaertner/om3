#include "om3lib.h"

#include <stdlib.h>
#include <string.h>

struct tuple
{
    om3obj header;
    size_t size;
    om3var items[];
};

static const om3ref NULLREF;

static om3var load_item(om3obj *obj, om3var key)
{
    struct tuple *tuple = (struct tuple *)obj;
    size_t idx = (size_t)om3_unbox_uint(key);
    assert(idx < tuple->size);
    return tuple->items[idx];
}

static void store_item(om3obj *obj, om3var key, om3var value)
{
    struct tuple *tuple = (struct tuple *)obj;
    size_t idx = (size_t)om3_unbox_uint(key);
    assert(idx < tuple->size);
    tuple->items[idx] = value;
}

static om3var invoke_type(om3obj *obj, size_t count, om3var *args)
{
    static const om3obj_vt VT = {
        om3_lib_fail_seek,
        load_item,
        store_item,
        om3_lib_fail_invoke,
        om3_lib_destroy_storage,
    };

    (void)obj;

    struct tuple *tuple= malloc(sizeof *tuple + count * sizeof *tuple->items);
    tuple->header.vt = &VT;
    tuple->size = count;
    memcpy(tuple->items, args, count * sizeof *tuple->items);

    return om3_box_obj(&tuple->header);
}

void om3_lib_init_tuple(om3obj **slot)
{
    static const om3obj_vt VT = {
        om3_lib_fail_seek,
        om3_lib_fail_load,
        om3_lib_fail_store,
        invoke_type,
        om3_lib_destroy_nothing,
    };

    static om3obj OBJ = { &VT };

    *slot = &OBJ;
}
