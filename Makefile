NOWARN := padded covered-switch-default float-equal

SOURCES := values.c lib.c tuple.c method.c class.c
HEADERS := om3.h om3lib.h
OBJECTS := $(SOURCES:%.c=%.o)

CLANG = clang -std=c99 -Werror -Weverything $(NOWARN:%=-Wno-%)
GCC = gcc -std=c99 -pedantic -ggdb3

.PHONY: build lib check clean realclean

build: $(OBJECTS)

lib: libom3.a

check: test.exe
	./test.exe

clean:
	rm -f $(OBJECTS)

realclean: clean
	rm -f test.exe libom3.a

libom3.a: $(OBJECTS)
	ar rcs $@ $^

test.exe: test.c libom3.a
	$(CLANG) -fsyntax-only $<
	$(GCC) -o $@ $^

$(OBJECTS): %.o: %.c $(HEADERS)
	$(CLANG) -fsyntax-only $<
	$(GCC) -c -o $@ $<
